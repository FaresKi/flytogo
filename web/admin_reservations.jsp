<%@ page import="Beans.Reservation" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/icon.png">
    <!-- Author Meta -->
    <meta name="author" content="colorlib">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>FlyToGo</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="css/linearicons.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<jsp:include page="header.jsp"/>
    <%List<Reservation> allReservations = (List<Reservation>) request.getAttribute("allReservations");%>
<!-- start banner Area -->
<section class="about-banner relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    FlyToGo - Administration des réservations
                </h1>
                <p class="text-white link-nav"><a href="index.jsp">Home </a> <span class="lnr lnr-arrow-right"></span>
                    <a href="admin.jsp"> Admin </a></p>
            </div>
        </div>
    </div>
</section>
<!-- End banner Area -->
<section class="about-info-area section-gap">
    <div class="container">
        <div class="row align-items-center">
            <form method="post" action="/admin_reservations" id="form_reservation"></form>
            <table class="table table-bordered table-striped" style="float: left;">
                <caption style="text-align: center; caption-side: top"><h2>Liste des réservations</h2></caption>
                <thead class="thead-dark">
                <tr>
                    <th scope="col">N°Vol</th>
                    <th scope="col">N°Utilisateur</th>
                    <th scope="col">Prix du vol</th>
                </tr>
                </thead>
                <tbody>
                <% for (Reservation reservation : allReservations) { %>
                <tr>
                    <th scope="row"><% out.println(reservation.getVol().getNumero_vol()); %></th>
                    <td><% out.println(reservation.getNumeroClient()); %></td>
                    <td><% out.println(reservation.getPrix_vol()); %> €</td>

                    <td align="center">
                        <a href="/admin_reservations?delete_reservation_util=<% out.println(reservation.getNumeroClient()); %>&delete_reservation_vol=<% out.println(reservation.getVol().getNumero_vol()); %>">
                            <span class="glyphicon glyphicon-trash"></span>
                            Supprimer </a>
                    </td>
                    <td align="center">

                    </td>
                </tr>
                <%}%>
                </tbody>
            </table>
        </div>
    </div>
</section>
</body>