<%@ page import="Beans.Reservation" %>
<!DOCTYPE html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<html lang="fr" class="no-js">
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/icon.png">
    <!-- Author Meta -->
    <meta name="author" content="colorlib">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->

    <title>FlyToGo</title>
    <% Reservation reservation = (Reservation) session.getAttribute("reservation"); %>
    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--CSS  ============================================= -->
    <link rel="stylesheet" href="css/linearicons.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="icon" href="img/icon.png">
</head>
<title>FlyToGo</title>
<jsp:include page="header.jsp"></jsp:include>
<section class="banner-area relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row fullscreen align-items-center justify-content-between">
            <div class="col-lg-6 col-md-6 banner-left">
                <h2 class="text-white"></h2>
                <br><br><br><br><br><br><br><br><br>
                <h2 class="text-white">Bravo ! Votre réservation a été effectuée!</h2>
                <p class="text-white">
                    Vous avez vu? C'était facile, simple et rapide. Que demande le peuple?
                </p>
            </div>
        </div>
    </div>
</section>

<section class="popular-destination-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <table class="table table-bordered table-striped">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">N° de vol</th>
                    <th scope="col">Aéroport de départ</th>
                    <th scope="col">Aéroport d'arrivée</th>
                    <th scope="col">Date d'aller - Date de retour</th>
                    <th scope="col">Prix</th>
                </tr>
                </thead>

                <tbody>
                <tr>
                    <th scope="row"><% out.println(reservation.getVol().getNumero_vol()); %></th>
                    <td><% out.println(reservation.getVol().getAeroportDep().replace("\"", "")); %></td>
                    <td><% out.println(reservation.getVol().getAeroportArr().replace("\"", "")); %></td>
                    <td><% out.println(reservation.getVol().getDate_dep()); %>
                        -<% out.println(reservation.getVol().getDate_arr()); %></td>
                    <td><% out.println(reservation.getPrix_vol()); %> €</td>
                </tr>

                </tbody>
            </table>
            <form method="post">
                <input type="submit" class="genric-btn btn-success"  value="Consultez vos réservations" href="/user">
            </form>

        </div>
    </div>

</section>


</html>
