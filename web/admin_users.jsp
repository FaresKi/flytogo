<%@ page import="Beans.Utilisateur" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/icon.png">
    <!-- Author Meta -->
    <meta name="author" content="colorlib">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>FlyToGo</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="css/linearicons.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<jsp:include page="header.jsp"/>
<%
    List<Utilisateur> allUsers = (List<Utilisateur>) request.getAttribute("allUsers");
%>
<!-- start banner Area -->
<section class="about-banner relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    FlyToGo - Administration des utilisateurs
                </h1>
                <p class="text-white link-nav"><a href="index.jsp">Home </a> <span class="lnr lnr-arrow-right"></span>
                    <a href="admin.jsp"> Admin </a></p>
            </div>
        </div>
    </div>
</section>
<!-- End banner Area -->

<!-- Start about-info Area -->
<section class="about-info-area section-gap">
    <div class="container">
        <div class="row align-items-center">
            <form method="post" id="form_user" action="/admin_users"></form>
            <table class="table table-bordered table-striped" style="float: left;">
                <caption style="text-align: center; caption-side: top"><h2>Liste des utilisateurs</h2></caption>
                <tr>
                    <th scope="row"><input type="text" class="form-control" name="id_user" readonly></th>
                    <td><input type="text" class="form-control" name="nom" form="form_user"></td>
                    <td><input type="text" class="form-control" name="prenom" form="form_user"></td>
                    <td><input type="text" class="form-control" name="email" form="form_user"></td>
                    <td><input type="text" class="form-control" name="type" readonly></td>
                </tr>
                <thead class="thead-dark">
                <tr>
                    <th scope="col">N°Utilisateur</th>
                    <th scope="col">NOM</th>
                    <th scope="col">Prénom</th>
                    <th scope="col">Email</th>
                    <th scope="col">Type</th>
                </tr>
                </thead>
                <tbody>
                <% for (Utilisateur user : allUsers) {%>
                <tr>
                    <th scope="row"><% out.println(user.getNumUtilsateur()); %></th>
                    <td><% out.println(user.getNom().toUpperCase()); %></td>
                    <td><% out.println(user.getPrenom()); %></td>
                    <td><% out.println(user.getEmail());%></td>
                    <td><% if (user.getTypeAdmin()) {
                        out.println("Admin");
                    } else {
                        out.println("Client");
                    }%></td>
                    <td align="center">
                        <button type="submit" class="btn btn-default btn-sm" name="modify_user" form="form_user"
                                onClick="window.location.reload();"
                                value="<% out.println(user.getNumUtilsateur()); %>"><span
                                class="glyphicon glyphicon-pencil"></span>Modifier
                        </button>
                        <br><br>
                        <button type="submit" class="btn btn-default btn-sm" name="delete_user" form="form_user"
                                onClick="window.location.reload();"
                                value="<% out.println(user.getNumUtilsateur()); %>"><span
                                class="glyphicon glyphicon-trash"></span>Supprimer
                        </button>
                    </td>
                </tr>
                <% } %>
                </tbody>
            </table>
        </div>
    </div>
    </div>
</section>
<!-- End about-info Area -->


<jsp:include page="footer.jsp"/>

<script src="js/vendor/jquery-2.2.4.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/easing.min.js"></script>
<script src="js/hoverIntent.js"></script>
<script src="js/superfish.min.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/mail-script.js"></script>
<script src="js/main.js"></script>
</body>
</html>