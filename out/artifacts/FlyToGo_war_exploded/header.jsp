<%@ page import="Beans.Utilisateur" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<!-- Mobile Specific Meta -->
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<!-- Favicon-->
<link rel="shortcut icon" href="img/icon.png">
<!-- Author Meta -->
<meta name="author" content="colorlib">
<!-- Meta Description -->
<meta name="description" content="">
<!-- Meta Keyword -->
<meta name="keywords" content="">
<!-- meta character set -->
<meta charset="UTF-8">
<!-- Site Title -->
<title>FlyToGo</title>
<link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
<link rel="shortcut icon" type="image/png" href="img/icon.png">
<!--CSS ============================================= -->
<link rel="stylesheet" href="css/linearicons.css">
<link rel="stylesheet" href="css/font-awesome.min.css">
<link rel="stylesheet" href="css/bootstrap.css">
<link rel="stylesheet" href="css/magnific-popup.css">
<link rel="stylesheet" href="css/jquery-ui.css">
<link rel="stylesheet" href="css/nice-select.css">
<link rel="stylesheet" href="css/animate.min.css">
<link rel="stylesheet" href="css/owl.carousel.css">
<link rel="stylesheet" href="css/main.css">
<header id="header">
    <title>FlyToGo</title>
    <div class="header-top">
        <div class="container">
            <div class="row align-items-center">
                <div class="col-lg-6 col-sm-6 col-6 header-top-left">
                    <ul>

                        <%Utilisateur loggedUser = (Utilisateur) session.getAttribute("loggedUser");%>
                        <% if (loggedUser != null) { %>
                        <li>
                            <a href="/user">
                                <button type="button" class="btn btn-primary" aria-label="Left Align"
                                        style="background: transparent;border: none">
                                    <span class="glyphicon glyphicon-user"
                                          style="background: transparent; font-size: small"> </span>
                                    Bonjour ${loggedUser.prenom}
                                </button>
                            </a>
                        </li>
                        <li><a href="/home">Acheter billets</a></li>
                        <% } else {%>
                        <li>
                            <a href="/login">
                                <button type="button" class="btn btn-primary" aria-label="Left Align"
                                        style="background: transparent;border: none">
                                    <span class="glyphicon glyphicon-user" style="background: transparent"> </span>
                                    Bonjour invité
                                </button>
                            </a>
                        </li>
                        <li><a href="/login">Acheter billets</a></li>
                        <% } %>
                    </ul>
                </div>
                <div class="col-lg-6 col-sm-6 col-6 header-top-right">
                    <div class="header-social">
                        <a href="https://www.facebook.com/poleemploi/"><i class="fa fa-facebook"></i></a>
                        <a href="https://twitter.com/poleemploi_IDF"><i class="fa fa-twitter"></i></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container main-menu">
        <div class="row align-items-center justify-content-between d-flex">
            <div id="logo">
                <a href="/home"><img src="img/logo4.png" height="60px" width="60px" alt="" title=""/></a>
            </div>
            <nav id="nav-menu-container">
                <ul class="nav-menu">
                    <li><a href="${pageContext.request.contextPath}/home">Accueil</a></li>
                    <li><a href="${pageContext.request.contextPath}/about">À propos</a></li>
                    <li><a href="${pageContext.request.contextPath}/packages">Paquets</a></li>
                    <li><a href="${pageContext.request.contextPath}/hotels">Hôtels</a></li>
                    <li><a href="${pageContext.request.contextPath}/insurance">Assurance</a></li>
                    <% if (loggedUser == null) {%>
                    <li><a href="${pageContext.request.contextPath}/login">Connexion</a></li>
                    <li><a href="${pageContext.request.contextPath}/signup">Inscription</a></li>
                    <%}%>
                    <li><a href="contact.jsp">Contact</a></li>
                    <% if (loggedUser != null && loggedUser.getTypeAdmin() == true) { %>
                    <li><a href="${pageContext.request.contextPath}/admin">Admin</a></li>
                    <%}%>
                    <% if (loggedUser != null) {%>
                    <li><a href="${pageContext.request.contextPath}/logout">Déconnexion</a></li>
                    <%}%>
                </ul>
            </nav><!-- #nav-menu-container -->
        </div>
    </div>
</header>
<!-- #header -->
