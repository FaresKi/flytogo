<%@ page import="Beans.Vol" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/icon.png">
    <!-- Author Meta -->
    <meta name="author" content="colorlib">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>FlyToGo</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="css/linearicons.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<jsp:include page="header.jsp"/>
<jsp:include page="header.jsp"/>

<!-- start banner Area -->
<section class="about-banner relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    FlyToGo - Administration
                </h1>
                <p class="text-white link-nav"><a href="index.jsp">Home </a> <span class="lnr lnr-arrow-right"></span>
                    <a href="/admin"> Admin </a></p>
            </div>
        </div>
    </div>
</section>
<!-- End banner Area -->
<section class="about-info-area section-gap">
    <div class="container">
        <div class="row align-items-center">
            <ul class="list-group">
                <li class="list-group-item">
                    <p class="text-black link-nav"><a href="/admin_users">Administration des utilisateurs </a>
                </li>
                <li class="list-group-item">
                    <p class="text-black link-nav"><a href="/admin_flights">Administration des vols </a>
                </li>
                <li class="list-group-item">
                    <p class="text-black link-nav"><a href="/admin_reservations">Administration des réservations </a>
                </li>
            </ul>
        </div>
    </div>
</section>
</body>