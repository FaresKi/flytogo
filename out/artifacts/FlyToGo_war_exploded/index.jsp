<!DOCTYPE html>
<%@page pageEncoding="UTF-8" contentType="text/html; charset=UTF-8" %>
<html lang="fr" class="no-js">
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/icon.png">
    <!-- Author Meta -->
    <meta name="author" content="colorlib">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->

    <title>FlyToGo</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--CSS  ============================================= -->
    <link rel="stylesheet" href="css/linearicons.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="icon" href="img/icon.png">
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
<!-- start banner Area -->
<section class="banner-area relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row fullscreen align-items-center justify-content-between">
            <div class="col-lg-6 col-md-6 banner-left">
                <h6 class="text-white">Echappez de la vie quotidienne</h6>
                <h1 class="text-white">Ne voyagez pas. Découvrez.</h1>
                <p class="text-white">
                    Vous découvrez les meilleurs tarifs de voyage dans le module de SI. Vous le savez.
                </p>
                <a href="login.jsp" class="primary-btn text-uppercase">Commencez.</a>
            </div>
            <div class="col-lg-4 col-md-6 banner-right">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="flight-tab" data-toggle="tab" href="#flight" role="tab"
                           aria-controls="flight" aria-selected="true">Flights</a>
                    </li>
                </ul>
                <div class="tab-content" id="myTabContent">
                    <div class="tab-pane fade show active" id="flight" role="tabpanel" aria-labelledby="flight-tab">
                        <form class="form-wrap" method="post">
                            <input type="text" class="form-control" name="aeroport_dep" list="airports"
                                   placeholder="Aéroport de départ "
                                   onfocus="this.placeholder = ''" onblur="this.placeholder = 'Aéroport de départ '">
                            <input type="text" class="form-control" name="aeroport_arr" list="airports"
                                   placeholder="Aéroport d'arrivée "
                                   onfocus="this.placeholder = ''" onblur="this.placeholder = 'Aéroport d\'arrivée '">
                            <input type="date" name="date_dep" class="form-control"
                                   placeholder="Date de départ "
                                   onfocus="this.placeholder = ''" onblur="this.placeholder = 'Date de départ '">
                            <input type="date" name="date_arr" class="form-control"
                                   placeholder="Date d'arrivée "
                                   onfocus="this.placeholder = ''" onblur="this.placeholder = 'Date d\'arrivée '">
                            <input type="number" min="0" max="20" class="form-control" name="nbr_adultes"
                                   placeholder="Adultes " onfocus="this.placeholder = ''"
                                   onblur="this.placeholder = 'Adultes '">
                            <input type="number" min="0" max="20" class="form-control" name="nbr_enfants"
                                   placeholder="Enfants "
                                   onfocus="this.placeholder = ''" onblur="this.placeholder = 'Enfants '">
                            <input type="submit" class="form-control" name="recherche" value="Envolez-vous."/>
                        </form>
                    </div>
                    <datalist id="airports">
                        <option>Adrar – Aéroport d'Adrar - Touat - Cheikh Sidi Mohamed Belkebir
                        <option>Alger – Aéroport d'Alger - Houari Boumédiène
                        <option>Annaba – Aéroport d'Annaba - Rabah-Bitat
                        <option>Batna – Aéroport de Batna - Mostepha Ben Boulaid
                        <option>Béjaïa – Aéroport de Béjaïa - Soummam - Abane Ramdane
                        <option>Biskra - Aéroport de Biskra - Mohamed Khider
                        <option>Chlef - Aéroport de Chlef
                        <option>Constantine – Aéroport de Constantine - Mohamed Boudiaf
                        <option>Djanet - Aéroport de Djanet - Tiska
                        <option>Hassi Messaoud – Aéroport d'Hassi Messaoud - Oued Irara - Krim Belkacem
                        <option>In Amenas - Aéroport de Zarzaïtine - In Amenas
                        <option>Jijel - Aéroport de Jijel - Ferhat Abbas
                        <option>Oran - Aéroport d'Oran - Ahmed Ben Bella (anc. Oran -Es Sénia)
                        <option>Sétif – Aéroport de Sétif - 8 Mai 1945
                        <option>Tamanrasset - Aéroport de Tamanrasset - Aguenar - Hadj Bey Akhamok
                        <option>Tlemcen – Aéroport de Tlemcen - Zenata - Messali El Hadj
                        <option>Ajaccio – Aéroport Napoléon-Bonaparte (Anc. Aéroport Campo dell'Oro)
                        <option>Bastia – Aéroport Poretta
                        <option>Beauvais - Aéroport Tillé
                        <option>Bergerac – Aéroport Dordogne-Périgord
                        <option>Béziers - Aéroport Cap d'Agde
                        <option>Biarritz/Bayonne - Aéroport Anglet
                        <option>Bordeaux – Aéroport Mérignac
                        <option>Brest – Aéroport Bretagne
                        <option>Calvi - Aéroport Sainte-Catherine
                        <option>Carcassonne – Aéroport Salvaza
                        <option>Châlons-en-Champagne – Aéroport Vatry
                        <option>Chambéry - Aéroport Savoie
                        <option>Clermont-Ferrand - Aéroport Auvergne
                        <option>Dinard – Aéroport Pleurtuit Saint-Malo
                        <option>Figari - Aéroport Sud Corse
                        <option>Grenoble - Aéroport Isère
                        <option>La Rochelle - Aéroport île de Ré
                        <option>Lille -Aéroport Lesquin
                        <option>Limoges – Aéroport Bellegarde
                        <option>Lyon – Aéroport Saint-Exupéry
                        <option>Marseille – Aéroport Provence
                        <option>Metz/Nancy - Aéroport Lorraine
                        <option>Montpellier - Aéroport Méditerranée
                        <option>Mulhouse – Aéroport international de Bâle-Mulhouse-Fribourg
                        <option>Nantes - Aéroport-Atlantique
                        <option>Nice – Aéroport Côte d'Azur
                        <option>Nîmes – Aéroport Garons
                        <option>Paris – Aéroport Roissy-CDG
                        <option>Paris – Aéroport Orly
                        <option>Pau – Aéroport Pau-Pyrénées
                        <option>Perpignan – Aéroport Rivesaltes
                        <option>Poitiers – Aéroport Biard
                        <option>Rennes - Aéroport de Rennes
                        <option>Rodez – Aéroport Aveyron
                        <option>Saint-Étienne - Aéroport Bouthéon
                        <option>Strasbourg - Aéroport Entzheim
                        <option>Tarbes/Lourdes - Aéroport Pyrénées
                        <option>Toulon – Aéroport Hyères
                        <option>Toulouse – Aéroport Blagnac
                        <option>Tours - Aéroport Val de Loire
                        <option>Antananarivo - Aéroport international d'Ivato
                        <option>Antsiranana - Aérodrome d'Arrachart
                        <option>Mahajanga - Aérodrome d'Amborovy
                        <option>Nosy Be - Aéroport international de Fascene
                        <option>Toamasina - Aéroport de Toamasina
                        <option>Tôlanaro - Aérodrome de Fort-Dauphin
                        <option>Toliara - Aéroport de Toliara
                    </datalist>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End banner Area -->

<!-- Start popular-destination Area -->
<section class="popular-destination-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-70 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">Popular Destinations</h1>
                    <p>We all live in an age that belongs to the young at heart. Life that is becoming extremely fast,
                        day.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="single-destination relative">
                    <div class="thumb relative">
                        <div class="overlay overlay-bg"></div>
                        <img class="img-fluid" src="img/d1.jpg" alt="">
                    </div>
                    <div class="desc">
                        <a href="#" class="price-btn">€150</a>
                        <h4>Mountain River</h4>
                        <p>Paraguay</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="single-destination relative">
                    <div class="thumb relative">
                        <div class="overlay overlay-bg"></div>
                        <img class="img-fluid" src="img/d2.jpg" alt="">
                    </div>
                    <div class="desc">
                        <a href="#" class="price-btn">€250</a>
                        <h4>Dream City</h4>
                        <p>Paris</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="single-destination relative">
                    <div class="thumb relative">
                        <div class="overlay overlay-bg"></div>
                        <img class="img-fluid" src="img/d3.jpg" alt="">
                    </div>
                    <div class="desc">
                        <a href="#" class="price-btn">€350</a>
                        <h4>Cloud Mountain</h4>
                        <p>Sri Lanka</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End popular-destination Area -->


<!-- Start price Area -->
<section class="price-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-70 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">We Provide Affordable Prices</h1>
                    <p>Well educated, intellectual people, especially scientists at all times demonstrate
                        considerably.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-4">
                <div class="single-price">
                    <h4>Cheap Packages</h4>
                    <ul class="price-list">
                        <li class="d-flex justify-content-between align-items-center">
                            <span>New York</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Maldives</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Sri Lanka</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Nepal</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Thailand</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Singapore</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="single-price">
                    <h4>Luxury Packages</h4>
                    <ul class="price-list">
                        <li class="d-flex justify-content-between align-items-center">
                            <span>New York</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Maldives</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Sri Lanka</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Nepal</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Thailand</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Singapore</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="single-price">
                    <h4>Camping Packages</h4>
                    <ul class="price-list">
                        <li class="d-flex justify-content-between align-items-center">
                            <span>New York</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Maldives</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Sri Lanka</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Nepal</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Thailand</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                        <li class="d-flex justify-content-between align-items-center">
                            <span>Singapore</span>
                            <a href="#" class="price-btn">€1500</a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End price Area -->


<!-- Start other-issue Area -->
<section class="other-issue-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-70 col-lg-9">
                <div class="title text-center">
                    <h1 class="mb-10">Other issues we can help you with</h1>
                    <p>We all live in an age that belongs to the young at heart. Life that is.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-6">
                <div class="single-other-issue">
                    <div class="thumb">
                        <img class="img-fluid" src="img/o1.jpg" alt="">
                    </div>
                    <a href="#">
                        <h4>Rent a Car</h4>
                    </a>
                    <p>
                        The preservation of human life is the ultimate value, a pillar of ethics and the foundation.
                    </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-other-issue">
                    <div class="thumb">
                        <img class="img-fluid" src="img/o2.jpg" alt="">
                    </div>
                    <a href="#">
                        <h4>Cruise Booking</h4>
                    </a>
                    <p>
                        I was always somebody who felt quite sorry for myself, what I had not got compared.
                    </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-other-issue">
                    <div class="thumb">
                        <img class="img-fluid" src="img/o3.jpg" alt="">
                    </div>
                    <a href="#">
                        <h4>To Do List</h4>
                    </a>
                    <p>
                        The following article covers a topic that has recently moved to center stage–at least it seems.
                    </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="single-other-issue">
                    <div class="thumb">
                        <img class="img-fluid" src="img/o4.jpg" alt="">
                    </div>
                    <a href="#">
                        <h4>Food Features</h4>
                    </a>
                    <p>
                        There are many kinds of narratives and organizing principles. Science is driven by evidence.
                    </p>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End other-issue Area -->


<!-- Start testimonial Area -->
<section class="testimonial-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-70 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">Testimonial from our Clients</h1>
                    <p>The French Revolution constituted for the conscience of the dominant aristocratic class a fall
                        from </p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="active-testimonial">
                <div class="single-testimonial item d-flex flex-row">
                    <div class="thumb">
                        <img class="img-fluid" src="img/elements/user1.png" alt="">
                    </div>
                    <div class="desc">
                        <p>
                            Do you want to be even more successful? Learn to love learning and growth. The more effort
                            you put into improving your skills, the bigger the payoff you.
                        </p>
                        <h4>Harriet Maxwell</h4>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                        </div>
                    </div>
                </div>
                <div class="single-testimonial item d-flex flex-row">
                    <div class="thumb">
                        <img class="img-fluid" src="img/elements/user2.png" alt="">
                    </div>
                    <div class="desc">
                        <p>
                            A purpose is the eternal condition for success. Every former smoker can tell you just how
                            hard it is to stop smoking cigarettes. However.
                        </p>
                        <h4>Carolyn Craig</h4>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                        </div>
                    </div>
                </div>
                <div class="single-testimonial item d-flex flex-row">
                    <div class="thumb">
                        <img class="img-fluid" src="img/elements/user1.png" alt="">
                    </div>
                    <div class="desc">
                        <p>
                            Do you want to be even more successful? Learn to love learning and growth. The more effort
                            you put into improving your skills, the bigger the payoff you.
                        </p>
                        <h4>Harriet Maxwell</h4>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                        </div>
                    </div>
                </div>
                <div class="single-testimonial item d-flex flex-row">
                    <div class="thumb">
                        <img class="img-fluid" src="img/elements/user2.png" alt="">
                    </div>
                    <div class="desc">
                        <p>
                            A purpose is the eternal condition for success. Every former smoker can tell you just how
                            hard it is to stop smoking cigarettes. However.
                        </p>
                        <h4>Carolyn Craig</h4>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                        </div>
                    </div>
                </div>
                <div class="single-testimonial item d-flex flex-row">
                    <div class="thumb">
                        <img class="img-fluid" src="img/elements/user1.png" alt="">
                    </div>
                    <div class="desc">
                        <p>
                            Do you want to be even more successful? Learn to love learning and growth. The more effort
                            you put into improving your skills, the bigger the payoff you.
                        </p>
                        <h4>Harriet Maxwell</h4>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                        </div>
                    </div>
                </div>
                <div class="single-testimonial item d-flex flex-row">
                    <div class="thumb">
                        <img class="img-fluid" src="img/elements/user2.png" alt="">
                    </div>
                    <div class="desc">
                        <p>
                            A purpose is the eternal condition for success. Every former smoker can tell you just how
                            hard it is to stop smoking cigarettes. However.
                        </p>
                        <h4>Carolyn Craig</h4>
                        <div class="star">
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star checked"></span>
                            <span class="fa fa-star"></span>
                            <span class="fa fa-star"></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- End testimonial Area -->

<!-- Start home-about Area -->
<section class="home-about-area">
    <div class="container-fluid">
        <div class="row align-items-center justify-content-end">
            <div class="col-lg-6 col-md-12 home-about-left">
                <h1>
                    Did not find your Package? <br>
                    Feel free to ask us. <br>
                    We‘ll make it for you
                </h1>
                <p>
                    inappropriate behavior is often laughed off as “boys will be boys,” women face higher conduct
                    standards especially in the workplace. That’s why it’s crucial that, as women, our behavior on the
                    job is beyond reproach. inappropriate behavior is often laughed.
                </p>
                <a href="#" class="primary-btn text-uppercase">request custom price</a>
            </div>
            <div class="col-lg-6 col-md-12 home-about-right no-padding">
                <img class="img-fluid" src="img/about-img.jpg" alt="">
            </div>
        </div>
    </div>
</section>
<!-- End home-about Area -->


<!-- Start blog Area -->
<section class="recent-blog-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-60 col-lg-9">
                <div class="title text-center">
                    <h1 class="mb-10">Latest from Our Blog</h1>
                    <p>With the exception of Nietzsche, no other madman has contributed so much to human sanity as
                        has.</p>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="active-recent-blog-carusel">
                <div class="single-recent-blog-post item">
                    <div class="thumb">
                        <img class="img-fluid" src="img/b1.jpg" alt="">
                    </div>
                    <div class="details">
                        <div class="tags">
                            <ul>
                                <li>
                                    <a href="#">Travel</a>
                                </li>
                                <li>
                                    <a href="#">Life Style</a>
                                </li>
                            </ul>
                        </div>
                        <a href="#"><h4 class="title">Low Cost Advertising</h4></a>
                        <p>
                            Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A
                            farmer.
                        </p>
                        <h6 class="date">31st January,2018</h6>
                    </div>
                </div>
                <div class="single-recent-blog-post item">
                    <div class="thumb">
                        <img class="img-fluid" src="img/b2.jpg" alt="">
                    </div>
                    <div class="details">
                        <div class="tags">
                            <ul>
                                <li>
                                    <a href="#">Travel</a>
                                </li>
                                <li>
                                    <a href="#">Life Style</a>
                                </li>
                            </ul>
                        </div>
                        <a href="#"><h4 class="title">Creative Outdoor Ads</h4></a>
                        <p>
                            Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A
                            farmer.
                        </p>
                        <h6 class="date">31st January,2018</h6>
                    </div>
                </div>
                <div class="single-recent-blog-post item">
                    <div class="thumb">
                        <img class="img-fluid" src="img/b3.jpg" alt="">
                    </div>
                    <div class="details">
                        <div class="tags">
                            <ul>
                                <li>
                                    <a href="#">Travel</a>
                                </li>
                                <li>
                                    <a href="#">Life Style</a>
                                </li>
                            </ul>
                        </div>
                        <a href="#"><h4 class="title">It's Classified How To Utilize Free</h4></a>
                        <p>
                            Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A
                            farmer.
                        </p>
                        <h6 class="date">31st January,2018</h6>
                    </div>
                </div>
                <div class="single-recent-blog-post item">
                    <div class="thumb">
                        <img class="img-fluid" src="img/b1.jpg" alt="">
                    </div>
                    <div class="details">
                        <div class="tags">
                            <ul>
                                <li>
                                    <a href="#">Travel</a>
                                </li>
                                <li>
                                    <a href="#">Life Style</a>
                                </li>
                            </ul>
                        </div>
                        <a href="#"><h4 class="title">Low Cost Advertising</h4></a>
                        <p>
                            Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A
                            farmer.
                        </p>
                        <h6 class="date">31st January,2018</h6>
                    </div>
                </div>
                <div class="single-recent-blog-post item">
                    <div class="thumb">
                        <img class="img-fluid" src="img/b2.jpg" alt="">
                    </div>
                    <div class="details">
                        <div class="tags">
                            <ul>
                                <li>
                                    <a href="#">Travel</a>
                                </li>
                                <li>
                                    <a href="#">Life Style</a>
                                </li>
                            </ul>
                        </div>
                        <a href="#"><h4 class="title">Creative Outdoor Ads</h4></a>
                        <p>
                            Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A
                            farmer.
                        </p>
                        <h6 class="date">31st January,2018</h6>
                    </div>
                </div>
                <div class="single-recent-blog-post item">
                    <div class="thumb">
                        <img class="img-fluid" src="img/b3.jpg" alt="">
                    </div>
                    <div class="details">
                        <div class="tags">
                            <ul>
                                <li>
                                    <a href="#">Travel</a>
                                </li>
                                <li>
                                    <a href="#">Life Style</a>
                                </li>
                            </ul>
                        </div>
                        <a href="#"><h4 class="title">It's Classified How To Utilize Free</h4></a>
                        <p>
                            Acres of Diamonds… you’ve read the famous story, or at least had it related to you. A
                            farmer.
                        </p>
                        <h6 class="date">31st January,2018</h6>
                    </div>
                </div>

            </div>
        </div>
    </div>
</section>
<!-- End recent-blog Area -->

<!-- start footer Area -->
<footer class="footer-area section-gap">
    <div class="container">

        <div class="row">
            <div class="col-lg-3  col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>About Agency</h6>
                    <p>
                        The world has become so fast paced that people don’t want to stand by reading a page of
                        information, they would much rather look at a presentation and understand the message. It has
                        come to a point
                    </p>
                </div>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>Navigation Links</h6>
                    <div class="row">
                        <div class="col">
                            <ul>
                                <li><a href="#">Home</a></li>
                                <li><a href="#">Feature</a></li>
                                <li><a href="#">Services</a></li>
                                <li><a href="#">Portfolio</a></li>
                            </ul>
                        </div>
                        <div class="col">
                            <ul>
                                <li><a href="#">Team</a></li>
                                <li><a href="#">Pricing</a></li>
                                <li><a href="#">Blog</a></li>
                                <li><a href="#">Contact</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3  col-md-6 col-sm-6">
                <div class="single-footer-widget">
                    <h6>Newsletter</h6>
                    <p>
                        For business professionals caught between high OEM price and mediocre print and graphic output.
                    </p>
                    <div id="mc_embed_signup">
                        <form target="_blank"
                              action="https://spondonit.us12.list-manage.com/subscribe/post?u=1462626880ade1ac87bd9c93a&amp;id=92a4423d01"
                              method="get" class="subscription relative">
                            <div class="input-group d-flex flex-row">
                                <input name="EMAIL" placeholder="Email Address" onfocus="this.placeholder = ''"
                                       onblur="this.placeholder = 'Email Address '" required="" type="email">
                                <button class="btn bb-btn"><span class="lnr lnr-location"></span></button>
                            </div>
                            <div class="mt-10 info"></div>
                        </form>
                    </div>
                </div>
            </div>
            <div class="col-lg-3  col-md-6 col-sm-6">
                <div class="single-footer-widget mail-chimp">
                    <h6 class="mb-20">InstaFeed</h6>
                    <ul class="instafeed d-flex flex-wrap">
                        <li><img src="img/i1.jpg" alt=""></li>
                        <li><img src="img/i2.jpg" alt=""></li>
                        <li><img src="img/i3.jpg" alt=""></li>
                        <li><img src="img/i4.jpg" alt=""></li>
                        <li><img src="img/i5.jpg" alt=""></li>
                        <li><img src="img/i6.jpg" alt=""></li>
                        <li><img src="img/i7.jpg" alt=""></li>
                        <li><img src="img/i8.jpg" alt=""></li>
                    </ul>
                </div>
            </div>
        </div>

        <div class="row footer-bottom d-flex justify-content-between align-items-center">
            <p class="col-lg-8 col-sm-12 footer-text m-0">
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. -->
                Copyright &copy;<script>document.write(new Date().getFullYear());</script>
                All rights reserved | This template is made with <i class="fa fa-heart-o" aria-hidden="true"></i> by <a
                    href="https://colorlib.com" target="_blank">Colorlib</a>
                <!-- Link back to Colorlib can't be removed. Template is licensed under CC BY 3.0. --></p>
            <div class="col-lg-4 col-sm-12 footer-social">
                <a href="#"><i class="fa fa-facebook"></i></a>
                <a href="#"><i class="fa fa-twitter"></i></a>
                <a href="#"><i class="fa fa-dribbble"></i></a>
                <a href="#"><i class="fa fa-behance"></i></a>
            </div>
        </div>
    </div>
</footer>
<!-- End footer Area -->

<script src="js/vendor/jquery-2.2.4.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/easing.min.js"></script>
<script src="js/hoverIntent.js"></script>
<script src="js/superfish.min.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/mail-script.js"></script>
<script src="js/main.js"></script>
</body>
</html>