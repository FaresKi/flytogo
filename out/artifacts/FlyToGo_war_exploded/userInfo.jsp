<%@ page import="Beans.Reservation" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/icon.png">
    <!-- Author Meta -->
    <meta name="author" content="colorlib">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>FlyToGo</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="css/linearicons.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/main.css">
</head>
</head>
<body>
<jsp:include page="header.jsp"></jsp:include>
<% List<Reservation> reservationList = (List<Reservation>) session.getAttribute("reservations"); %>

<!-- start banner Area -->
<section class="about-banner relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    <h1 class="mb-10"><b>Bienvenue, ${loggedUser.prenom} ${loggedUser.nom}</b></h1>
                </h1>
            </div>
        </div>
    </div>
</section>
<!-- End banner Area -->

<!-- Start destinations Area -->
<section class="destinations-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <div class="menu-content pb-40 col-lg-8">
                <div class="title text-center">
                    <h1 class="mb-10">Vos Réservations</h1>
                    <p>Vous retrouvez ici vos réservations de voyages.</p>
                </div>
            </div>
        </div>
        <% if (reservationList.isEmpty()) { %>
        <p>Vous n'avez pas de réservations en cours</p>
        <% } %>
        <div class="row">
            <%for (Reservation reservation : reservationList) { %>
            <div class="col-lg-4">
                <div class="single-destinations">
                    <div class="thumb">
                        <img src="img/hotels/plane.jpg" alt="">
                    </div>
                    <div class="details">
                        <h4 class="d-flex justify-content-between">
                            <span>N° de vol : <%out.println(reservation.getVol().getNumero_vol()); %></span>
                        </h4>
                        <ul class="package-list">
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Aéroport de départ</span>
                                <span><% out.println(reservation.getVol().getAeroportDep()); %></span>
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Aéroport d'arrivée</span>
                                <span><% out.println(reservation.getVol().getAeroportArr()); %></span>
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Date de départ</span>
                                <span><% out.println(reservation.getVol().getDate_dep()); %></span>
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Date d'arrivée</span>
                                <span><% out.println(reservation.getVol().getDate_arr()); %></span>
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Nombre de passagers dans le vol</span>
                                <span><% out.println(reservation.getVol().getNombrePassagers());%></span>
                            </li>
                            <li class="d-flex justify-content-between align-items-center">
                                <span>Prix</span>
                                <span><% out.println(reservation.getPrix_vol());%> €</span>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
            <% }%>
        </div>
    </div>
</section>
<!-- End destinations Area -->

<jsp:include page="footer.jsp"></jsp:include>
</body>
</html>
