<%@ page import="Beans.Vol" %>
<%@ page import="java.util.List" %><%--
  Created by IntelliJ IDEA.
  User: fkissoum
  Date: 07/07/2019
  Time: 15:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<jsp:include page="header.jsp"></jsp:include>
<%
    List<Vol> flightList = (List<Vol>) session.getAttribute("flightList");
%>
<html lang="fr" class="no-js">
<meta http-equiv='Content-Type' content='text/html; charset=UTF-8'/>
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/icon.png">
    <!-- Author Meta -->
    <meta name="author" content="colorlib">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->

    <title>FlyToGo</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--CSS  ============================================= -->
    <link rel="stylesheet" href="css/linearicons.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/main.css">
    <link rel="icon" href="img/icon.png">
</head>
<jsp:include page="header.jsp"></jsp:include>

<section class="banner-area relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row fullscreen align-items-center justify-content-between">
            <div class="col-lg-6 col-md-6 banner-left">
                <h2 class="text-white"></h2>
                <br><br><br><br><br><br><br><br><br>
                <h2 class="text-white">Résultats de votre recherche.</h2>
                <p class="text-white">
                    Trouvez les meilleurs tarifs. On vous rembourse le triple de la différence si vous trouvez mieux
                    ailleurs.
                    (Vous ne trouverez pas, en tout cas on espère.)
                </p>
            </div>
        </div>
    </div>
</section>

<section class="popular-destination-area section-gap">
    <div class="container">
        <div class="row d-flex justify-content-center">
            <table class="table table-bordered table-striped">
                <thead class="thead-dark">
                <tr>
                    <th scope="col">N° de vol</th>
                    <th scope="col">Aéroport de départ</th>
                    <th scope="col">Aéroport d'arrivée</th>
                    <th scope="col">Date d'aller - Date de retour</th>
                    <th scope="col">Prix par passager</th>
                </tr>
                </thead>

                <tbody>
                <% for (Vol vol : flightList) {
                %>
                <tr>
                    <form class=form-wrap" method="post" id="search">
                        <th scope="row"><% out.println(vol.getNumero_vol()); %></th>
                        <td><% out.println(vol.getAeroportDep().replace("\"", "")); %></td>
                        <td><% out.println(vol.getAeroportArr().replace("\"", "")); %></td>
                        <td><% out.println(vol.getDate_dep()); %>-<% out.println(vol.getDate_arr()); %></td>
                        <td><% out.println(vol.getPrix()); %> €</td>
                    </form>
                    <td>
                        <button type="submit" class="btn btn-default btn-sm" form="search" name="validate_reservation"
                                value="<% out.println(vol.getNumero_vol()); %>">
                            <span class="glyphicon glyphicon-ok"></span>Validez
                        </button>
                    </td>
                </tr>
                <% }%>
                </tbody>
            </table>
        </div>
    </div>
</section>
</html>
