<%@ page import="Beans.Reservation" %>
<%@ page import="Beans.Utilisateur" %>
<%@ page import="Beans.Vol" %>
<%@ page import="java.util.List" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<!DOCTYPE html>
<html lang="zxx" class="no-js">
<head>
    <!-- Mobile Specific Meta -->
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <!-- Favicon-->
    <link rel="shortcut icon" href="img/icon.png">
    <!-- Author Meta -->
    <meta name="author" content="colorlib">
    <!-- Meta Description -->
    <meta name="description" content="">
    <!-- Meta Keyword -->
    <meta name="keywords" content="">
    <!-- meta character set -->
    <meta charset="UTF-8">
    <!-- Site Title -->
    <title>FlyToGo</title>

    <link href="https://fonts.googleapis.com/css?family=Poppins:100,200,400,300,500,600,700" rel="stylesheet">
    <!--
    CSS
    ============================================= -->
    <link rel="stylesheet" href="css/linearicons.css">
    <link rel="stylesheet" href="css/font-awesome.min.css">
    <link rel="stylesheet" href="css/bootstrap.css">
    <link rel="stylesheet" href="css/magnific-popup.css">
    <link rel="stylesheet" href="css/jquery-ui.css">
    <link rel="stylesheet" href="css/nice-select.css">
    <link rel="stylesheet" href="css/animate.min.css">
    <link rel="stylesheet" href="css/owl.carousel.css">
    <link rel="stylesheet" href="css/main.css">
</head>
<body>
<jsp:include page="header.jsp"/>
    <% List<Vol> allFlights = (List<Vol>) request.getAttribute("allflights");
%>
<!-- start banner Area -->
<section class="about-banner relative">
    <div class="overlay overlay-bg"></div>
    <div class="container">
        <div class="row d-flex align-items-center justify-content-center">
            <div class="about-content col-lg-12">
                <h1 class="text-white">
                    FlyToGo - Administration des vols
                </h1>
                <p class="text-white link-nav"><a href="index.jsp">Home </a> <span class="lnr lnr-arrow-right"></span>
                    <a href="admin.jsp"> Admin </a></p>
            </div>
        </div>
    </div>
</section>
<!-- End banner Area -->
<section class="about-info-area section-gap">
    <div class="container">
        <div class="row align-items-center">
            <form method="post" action="/admin" id="form_flight"></form>
            <table class="table table-bordered table-striped" style="float: left;">
                <caption style="text-align: center; caption-side: top"><h2>Liste des vols</h2></caption>
                <tr>

                    <th scope="row"><input type="number" class="form-control" name="id_vol" readonly form="form_flight">
                    </th>
                    <td><input type="text" class="form-control" name="aeroport_dep" list="airports" form="form_flight">
                    </td>
                    <td><input type="text" class="form-control" name="aeroport_arr" list="airports" form="form_flight">
                    </td>
                    <td><input type="date" name="date_dep" class="form-control"
                               placeholder="Date de départ "
                               onfocus="this.placeholder = ''" onblur="this.placeholder = 'Date de départ '"></td>
                    <td><input type="date" name="date_arr" class="form-control"
                               placeholder="Date d'arrivée "
                               onfocus="this.placeholder = ''" onblur="this.placeholder = 'Date d\'arrivée '"></td>
                    <td><input type="text" class="form-control" name="prix_passager" form="form_flight"></td>
                </tr>
                <thead class="thead-dark">
                <tr>
                    <th scope="col">N°Vol</th>
                    <th scope="col">Aéroport de départ</th>
                    <th scope="col">Aéroport d'arrivée</th>
                    <th scope="col">Date de départ</th>
                    <th scope="col">Date d'arrivée</th>
                    <th scope="col">Prix par personne</th>
                </tr>
                </thead>
                <tbody>
                <% for (Vol vol : allFlights) { %>
                <tr>
                    <th scope="row"><% out.println(vol.getNumero_vol()); %></th>
                    <td><% out.println(vol.getAeroportDep()); %></td>
                    <td><% out.println(vol.getAeroportArr()); %></td>
                    <td><% out.println(vol.getDate_dep().toString()); %></td>
                    <td><% out.println(vol.getDate_arr().toString()); %></td>
                    <td><% out.println(vol.getPrix()); %> €</td>
                    <td align="center">
                        <button type="submit" class="btn btn-default btn-sm" name="delete_flight" form="form_flight"
                                onClick="window.location.reload();"
                                value="<%out.println(vol.getNumero_vol()); %>"><span class="glyphicon glyphicon-trash"></span>Supprimer
                        </button>
                        <br><br>
                        <button type="submit" class="btn btn-default btn-sm" name="modify_flight" form="form_flight"
                                onClick="window.location.reload();"
                                value="<% out.println(vol.getNumero_vol()); %>"><span class="glyphicon glyphicon-pencil"></span>Modifier
                        </button>
                    </td>
                </tr>
                <% } %>
                </tbody>
            </table>
        </div>
    </div>
</section>
</body>
<datalist id="airports">
    <option>Adrar – Aéroport d'Adrar - Touat - Cheikh Sidi Mohamed Belkebir
    <option>Alger – Aéroport d'Alger - Houari Boumédiène
    <option>Annaba – Aéroport d'Annaba - Rabah-Bitat
    <option>Batna – Aéroport de Batna - Mostepha Ben Boulaid
    <option>Béjaïa – Aéroport de Béjaïa - Soummam - Abane Ramdane
    <option>Biskra - Aéroport de Biskra - Mohamed Khider
    <option>Chlef - Aéroport de Chlef
    <option>Constantine – Aéroport de Constantine - Mohamed Boudiaf
    <option>Djanet - Aéroport de Djanet - Tiska
    <option>Hassi Messaoud – Aéroport d'Hassi Messaoud - Oued Irara - Krim Belkacem
    <option>In Amenas - Aéroport de Zarzaïtine - In Amenas
    <option>Jijel - Aéroport de Jijel - Ferhat Abbas
    <option>Oran - Aéroport d'Oran - Ahmed Ben Bella (anc. Oran -Es Sénia)
    <option>Sétif – Aéroport de Sétif - 8 Mai 1945
    <option>Tamanrasset - Aéroport de Tamanrasset - Aguenar - Hadj Bey Akhamok
    <option>Tlemcen – Aéroport de Tlemcen - Zenata - Messali El Hadj
    <option>Ajaccio – Aéroport Napoléon-Bonaparte (Anc. Aéroport Campo dell'Oro)
    <option>Bastia – Aéroport Poretta
    <option>Beauvais - Aéroport Tillé
    <option>Bergerac – Aéroport Dordogne-Périgord
    <option>Béziers - Aéroport Cap d'Agde
    <option>Biarritz/Bayonne - Aéroport Anglet
    <option>Bordeaux – Aéroport Mérignac
    <option>Brest – Aéroport Bretagne
    <option>Calvi - Aéroport Sainte-Catherine
    <option>Carcassonne – Aéroport Salvaza
    <option>Châlons-en-Champagne – Aéroport Vatry
    <option>Chambéry - Aéroport Savoie
    <option>Clermont-Ferrand - Aéroport Auvergne
    <option>Dinard – Aéroport Pleurtuit Saint-Malo
    <option>Figari - Aéroport Sud Corse
    <option>Grenoble - Aéroport Isère
    <option>La Rochelle - Aéroport île de Ré
    <option>Lille -Aéroport Lesquin
    <option>Limoges – Aéroport Bellegarde
    <option>Lyon – Aéroport Saint-Exupéry
    <option>Marseille – Aéroport Provence
    <option>Metz/Nancy - Aéroport Lorraine
    <option>Montpellier - Aéroport Méditerranée
    <option>Mulhouse – Aéroport international de Bâle-Mulhouse-Fribourg
    <option>Nantes - Aéroport-Atlantique
    <option>Nice – Aéroport Côte d'Azur
    <option>Nîmes – Aéroport Garons
    <option>Paris – Aéroport Roissy-CDG
    <option>Paris – Aéroport Orly
    <option>Pau – Aéroport Pau-Pyrénées
    <option>Perpignan – Aéroport Rivesaltes
    <option>Poitiers – Aéroport Biard
    <option>Rennes - Aéroport de Rennes
    <option>Rodez – Aéroport Aveyron
    <option>Saint-Étienne - Aéroport Bouthéon
    <option>Strasbourg - Aéroport Entzheim
    <option>Tarbes/Lourdes - Aéroport Pyrénées
    <option>Toulon – Aéroport Hyères
    <option>Toulouse – Aéroport Blagnac
    <option>Tours - Aéroport Val de Loire
    <option>Antananarivo - Aéroport international d'Ivato
    <option>Antsiranana - Aérodrome d'Arrachart
    <option>Mahajanga - Aérodrome d'Amborovy
    <option>Nosy Be - Aéroport international de Fascene
    <option>Toamasina - Aéroport de Toamasina
    <option>Tôlanaro - Aérodrome de Fort-Dauphin
    <option>Toliara - Aéroport de Toliara
</datalist>


<jsp:include page="footer.jsp"/>

<script src="js/vendor/jquery-2.2.4.min.js"></script>
<script src="js/popper.min.js"></script>
<script src="js/vendor/bootstrap.min.js"></script>
<script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBhOdIF3Y9382fqJYt5I_sswSrEw5eihAA"></script>
<script src="js/jquery-ui.js"></script>
<script src="js/easing.min.js"></script>
<script src="js/hoverIntent.js"></script>
<script src="js/superfish.min.js"></script>
<script src="js/jquery.ajaxchimp.min.js"></script>
<script src="js/jquery.magnific-popup.min.js"></script>
<script src="js/jquery.nice-select.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/mail-script.js"></script>
<script src="js/main.js"></script>
</body>
</html>