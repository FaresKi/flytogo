package Beans;

public class Reservation {
    private int numeroClient;
    private Vol vol;
    public int prix_vol;

    public Reservation(int numeroClient, Vol vol, int prix_vol) {
        this.numeroClient = numeroClient;
        this.vol = vol;
        this.prix_vol = prix_vol;
    }

    public Reservation(int numeroClient, Vol vol) {
        this.numeroClient = numeroClient;
        this.vol = vol;
    }

    public int getPrix_vol() {
        return prix_vol;
    }

    public int getNumeroClient() {
        return numeroClient;
    }

    public void setNumeroClient(int numeroClient) {
        this.numeroClient = numeroClient;
    }

    public Vol getVol() {
        return vol;
    }

    public void setVol(Vol vol) {
        this.vol = vol;
    }
}
