package Beans;

public class Utilisateur {

    private final int numUtilsateur;
    private final String nom;
    private final String prenom;
    private final String email;
    private final String password;
    private final boolean typeAdmin;

    public Utilisateur(int numUtilsateur, String nom, String prenom, String email, String password, boolean typeAdmin) {
        this.numUtilsateur = numUtilsateur;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.password = password;
        this.typeAdmin = typeAdmin;
    }

    public int getNumUtilsateur() {
        return numUtilsateur;
    }

    public String getNom() {
        return nom;
    }


    public String getPrenom() {
        return prenom;
    }


    public boolean getTypeAdmin() {
        return typeAdmin;
    }

    public String getEmail() {
        return email;
    }

    public String getPassword() {
        return password;
    }

}
