package Beans;


import java.sql.Date;

public class Vol {

    private String aeroportDep;
    private String aeroportArr;
    private Date date_dep;
    private Date date_arr;
    private int nombrePassagers;
    private int numero_vol;
    private int prix;

    public Vol(int numero_vol, String aeroportDep, String aeroportArr, Date date_dep, Date date_arr, int nombrePassagers, int prix) {
        this.aeroportDep = aeroportDep;
        this.aeroportArr = aeroportArr;
        this.date_dep = date_dep;
        this.date_arr = date_arr;
        this.nombrePassagers = nombrePassagers;
        this.numero_vol = numero_vol;
        this.prix = prix;
    }

    public int getPrix() {
        return prix;
    }

    public int getNumero_vol() {
        return numero_vol;
    }

    public String getAeroportDep() {
        return aeroportDep;
    }

    public String getAeroportArr() {
        return aeroportArr;
    }

    public Date getDate_dep() {
        return date_dep;
    }

    public Date getDate_arr() {
        return date_arr;
    }

    public int getNombrePassagers() {
        return nombrePassagers;
    }

}
