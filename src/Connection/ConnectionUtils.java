package Connection;

import com.mysql.cj.jdbc.MysqlDataSource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class ConnectionUtils {
    public static Connection getMyConnection() throws SQLException, ClassNotFoundException{
        Connection conn;
        MysqlDataSource dataSource = new MysqlDataSource();
        dataSource.setUser("fares");
        dataSource.setPassword("fares");
        dataSource.setDatabaseName("flytogo");
        dataSource.setServerName("localhost");
        dataSource.setPort(3306);
        dataSource.setServerTimezone("Europe/Paris");
        conn = dataSource.getConnection();
        return conn;
    }
    public static void closeQuietly(Connection conn)
    {
        try{
            conn.close();
        }catch(Exception e){

        }
    }
    public static void rollbackQuietly(Connection conn){
        try{
            conn.rollback();
        }catch(Exception e){

        }
    }

    public static void main(String[] args) throws SQLException, ClassNotFoundException {
        System.out.println("Get connection...");
        Connection conn = ConnectionUtils.getMyConnection();
        System.out.println("Get connection " + conn);
        System.out.println("Done!");
    }
}
