package Servlets;

import Beans.Reservation;
import Beans.Utilisateur;
import Beans.Vol;
import Utils.DBUtils;
import Utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

@WebServlet(urlPatterns = {"/admin"})
public class AdminServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Forward à WEB-INF/views/homeView.jsp
        RequestDispatcher dispatcher;
        HttpSession session = request.getSession();
        Utilisateur utilisateur = MyUtils.getLoggedUser(session);
        if (utilisateur != null) {
            if (utilisateur.getTypeAdmin()) {
                dispatcher = this.getServletContext().getRequestDispatcher("/admin.jsp");
            } else {
                dispatcher = this.getServletContext().getRequestDispatcher("/login");
            }
            dispatcher.forward(request, response);
        } else this.getServletContext().getRequestDispatcher("/login");

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doGet(request,response);
    }


}
