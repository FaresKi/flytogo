package Servlets;

import Beans.Reservation;
import Beans.Utilisateur;
import Utils.DBUtils;
import Utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.util.List;


@WebServlet(urlPatterns = {"/user"})
public class UserServlet extends HttpServlet {

    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Forward à WEB-INF/views/homeView.jsp

        HttpSession session = request.getSession();
        Utilisateur user = MyUtils.getLoggedUser(session);
        if (user != null) {
            Connection conn = MyUtils.getStoredConnection(request);
            try {
                List<Reservation> reservations = DBUtils.findReservations(conn, user.getNumUtilsateur());
                if(reservations==null){
                    System.out.println("Liste vide");
                }
                session.setAttribute("reservations", reservations);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/userInfo.jsp");
        dispatcher.forward(request, response);
    }



    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doGet(request, response);
    }
}
