package Servlets;

import Beans.Reservation;
import Beans.Utilisateur;
import Utils.DBUtils;
import Utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@WebServlet(urlPatterns = {"/admin_reservations"})
public class AdminReservationServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Forward à WEB-INF/views/homeView.jsp
        RequestDispatcher dispatcher;
        HttpSession session = request.getSession();
        Utilisateur utilisateur = MyUtils.getLoggedUser(session);
        Connection connection = MyUtils.getStoredConnection(request);
        try {
            List<Reservation> allReservations = DBUtils.allReservations(connection);
            request.setAttribute("allReservations", allReservations);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (utilisateur != null) {
            if (utilisateur.getTypeAdmin()) {
                dispatcher = this.getServletContext().getRequestDispatcher("/admin_reservations.jsp");
            } else {
                dispatcher = this.getServletContext().getRequestDispatcher("/login");
            }
            dispatcher.forward(request, response);
        } else this.getServletContext().getRequestDispatcher("/login");

        System.out.println(request.getParameter("delete_reservation_util"));
        System.out.println(request.getParameter("delete_reservation_vol"));
        if (request.getParameter("delete_reservation_util") != null && request.getParameter("delete_reservation_vol") != null) {
            int delete_reservation_util = Integer.parseInt(request.getParameter("delete_reservation_util").trim());
            int delete_reservation_vol = Integer.parseInt(request.getParameter("delete_reservation_vol").trim());
            System.out.println("Util : " + delete_reservation_util);
            System.out.println("Vol : " + delete_reservation_vol);

            try {
                DBUtils.deleteReservation(connection, delete_reservation_util, delete_reservation_vol);
                response.setIntHeader("Refresh", (int) 0.5);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        doGet(request, response);


    }


}
