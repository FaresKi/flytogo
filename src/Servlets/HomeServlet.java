package Servlets;

import Beans.Utilisateur;
import Beans.Vol;
import Utils.DBUtils;
import Utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

@WebServlet(urlPatterns = {"/home"})
public class HomeServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/index.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        request.setCharacterEncoding("UTF-8");
        String aeroport_dep = request.getParameter("aeroport_dep");
        String aeroport_arr = request.getParameter("aeroport_arr");
        Date date_dep = Date.valueOf(request.getParameter("date_dep"));
        Date date_arr = Date.valueOf(request.getParameter("date_arr"));
        int nbr_adultes = Integer.parseInt(request.getParameter("nbr_adultes"));
        int nbr_enfants = Integer.parseInt(request.getParameter("nbr_enfants"));
        String errorMessage;
        if(aeroport_arr.isEmpty() || aeroport_dep.isEmpty()|| date_dep==null || date_arr==null||aeroport_dep.equals(aeroport_arr) || date_arr.before(date_dep)){
            errorMessage="Refaites votre réservation";
        }


        HttpSession session = request.getSession();
        session.setAttribute("nbr_enfants",nbr_enfants);
        session.setAttribute("nbr_adultes",nbr_adultes);
        Utilisateur user = MyUtils.getLoggedUser(session);
        if (user == null) {
            System.out.println("User inexistant");
            response.sendRedirect("/login");
        } else {
            Connection conn = MyUtils.getStoredConnection(request);
            try {
                System.out.println("User found");
                List<Vol> flightResults = DBUtils.findFlights(user,conn, aeroport_dep, aeroport_arr, date_dep, date_arr, nbr_adultes, nbr_enfants);
                MyUtils.storeFlights(session,flightResults);
                if(!flightResults.isEmpty() || flightResults==null){
                    response.sendRedirect("/flight_search_results");
                }
               else{
                    response.sendRedirect("/home");
                }
            } catch (SQLException e) {
                response.sendRedirect("/home");
                e.printStackTrace();
            }
        }
    }
}
