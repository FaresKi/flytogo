package Servlets;

import Utils.DBUtils;
import Utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.Connection;

@WebServlet(urlPatterns = {"/signup"})

public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Forward à WEB-INF/views/homeView.jsp
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/signup.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String prenom = request.getParameter("prenom");
        String nom = request.getParameter("nom");
        String email = request.getParameter("email");
        String password = request.getParameter("password");

        String confirmpassword = request.getParameter("confirmpassword");
        boolean hasError = true;
        String errorString;
        if (nom == null || prenom == null || email == null || password == null || confirmpassword == null || nom.isEmpty() || prenom.isEmpty() || email.isEmpty() || password.isEmpty() || confirmpassword.isEmpty() || !password.equals(confirmpassword)) {
            hasError = true;
            errorString = "Required username and password";
            if (!password.equals(confirmpassword)) {
                errorString = "Passwords aren't the same";
            }
        } else {
            Connection conn = MyUtils.getStoredConnection(request);
            try {
                DBUtils.createUser(conn, nom, prenom, email, password);
            } catch (Exception e) {
                e.printStackTrace();
                hasError = true;
                errorString = e.getMessage();
            }
        }
        if (hasError) {
            RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/signup.jsp");

        }
        response.sendRedirect(request.getContextPath() + "/login");


    }
}
