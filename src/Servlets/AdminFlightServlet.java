package Servlets;

import Beans.Utilisateur;
import Beans.Vol;
import Utils.DBUtils;
import Utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.SQLException;
import java.util.List;

@WebServlet(urlPatterns = {"/admin_flights"})

public class AdminFlightServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Forward à WEB-INF/views/homeView.jsp
        RequestDispatcher dispatcher;
        HttpSession session = request.getSession();
        Utilisateur utilisateur = MyUtils.getLoggedUser(session);
        Connection connection = MyUtils.getStoredConnection(request);
        try {
            List<Vol> allFlights = DBUtils.allFlights(connection);
            request.setAttribute("allflights", allFlights);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (utilisateur != null) {
            if (utilisateur.getTypeAdmin()) {
                dispatcher = this.getServletContext().getRequestDispatcher("/admin_flights.jsp");
            } else {
                dispatcher = this.getServletContext().getRequestDispatcher("/login");
            }
            dispatcher.forward(request, response);
        } else this.getServletContext().getRequestDispatcher("/login");

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        Connection conn = MyUtils.getStoredConnection(request);
        request.setCharacterEncoding("UTF-8");
        Vol vol = null;
        if (request.getParameter("modify_flight") != null) {
            int modify_flight = Integer.parseInt(request.getParameter("modify_flight").trim());
            try {
                vol = DBUtils.findFlight(conn, modify_flight);
            } catch (SQLException e) {
                e.printStackTrace();
            }
            String aeroport_dep = request.getParameter("aeroport_dep");
            String aeroport_arr = request.getParameter("aeroport_arr");
            Date date_dep = null;
            Date date_arr = null;
            int prix_passager;

            if (aeroport_dep.isEmpty()) {
                aeroport_dep = vol.getAeroportDep();
            }

            if (aeroport_arr.isEmpty()) {
                aeroport_arr = vol.getAeroportArr();
            }
            if (request.getParameter("date_dep") == null) {
                date_dep = vol.getDate_dep();
            } else {
                date_dep = Date.valueOf(request.getParameter("date_dep"));
            }
            if (request.getParameter("date_arr") == null) {
                date_arr = vol.getDate_arr();
            } else {
                date_arr = Date.valueOf(request.getParameter("date_arr"));
            }
            if (request.getParameter("prix_passager")==null ||request.getParameter("prix_passager").isEmpty()) {
                prix_passager = vol.getPrix();
            }  else {
                System.out.println("Prix : " + request.getParameter("prix_passager"));
                prix_passager = Integer.parseInt(request.getParameter("prix_passager").trim());
            }

            try {
                DBUtils.modifyFlight(conn, aeroport_dep, aeroport_arr, date_dep, date_arr, prix_passager, modify_flight);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (request.getParameter("delete_flight") != null) {
            int delete_flight = Integer.parseInt(request.getParameter("delete_flight").trim());

            try {
                DBUtils.deleteFlight(conn, delete_flight);
                response.setIntHeader("Refresh", (int) 0.5);
            } catch (SQLException e) {
                e.printStackTrace();
            }

        }

    }

}
