package Servlets;

import Beans.Reservation;
import Beans.Utilisateur;
import Beans.Vol;
import Utils.DBUtils;
import Utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;


@WebServlet(urlPatterns = {"/flight_search_results"})
public class SearchResults extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {


        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/search.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        int rowid = Integer.parseInt(request.getParameter("validate_reservation").trim());
        int nbr_adultes = (int) request.getSession().getAttribute("nbr_adultes");
        int nbr_enfants = (int) request.getSession().getAttribute("nbr_enfants");
        System.out.println("Row ID :" + rowid);
        HttpSession session = request.getSession();
        Utilisateur user = MyUtils.getLoggedUser(session);
        Connection connection = MyUtils.getStoredConnection(request);
        if (user != null) {
            try {
                Vol vol = DBUtils.findFlight(connection, rowid);
                Reservation reservation = DBUtils.createReservation(connection, user.getNumUtilsateur(), vol, nbr_adultes + nbr_enfants);
                if (vol != null && reservation != null) {
                    MyUtils.storeReservation(session, reservation);
                    response.sendRedirect("/reservation");
                } else {
                    System.out.println("ça trouve pas");
                }


            } catch (SQLException e) {
                response.sendRedirect("/home");
                e.printStackTrace();
            }
        }

    }
}
