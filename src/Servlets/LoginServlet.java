package Servlets;

import Beans.Utilisateur;
import Utils.DBUtils;
import Utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;

@WebServlet(urlPatterns = {"/login"})
public class LoginServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher dispatcher = this.getServletContext().getRequestDispatcher("/login.jsp");
        dispatcher.forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String email = request.getParameter("connexion_email");
        String password = request.getParameter("connexion_password");
        String rememberMeStr = request.getParameter("rememberMe");
        boolean remember="Y".equals(rememberMeStr);
        Utilisateur user = null;
        boolean hasError = false;
        String errorString = null;
        if(email==null||password==null||email.length()==0||password.length()==0){
            hasError=true;
            errorString = "Required username and password!";
            System.out.println(errorString);
        }else {
            Connection conn = MyUtils.getStoredConnection(request);
            try{
                user= DBUtils.findUser(conn,email,password);
                if(user==null){
                    hasError=true;
                    errorString="Email or password invalid";
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        if(hasError){
            request.setAttribute("errorString",errorString);
            System.out.println(errorString);
            RequestDispatcher dispatcher=this.getServletContext().getRequestDispatcher("/login.jsp");
            dispatcher.forward(request,response);
        }
        else{
            HttpSession session = request.getSession();
            MyUtils.storedLoggedUser(session,user);
            if(remember){
                MyUtils.storeUserCookie(response,user);
                response.sendRedirect(request.getContextPath()+"/user");
            }
            else{
                response.sendRedirect(request.getContextPath()+"/home");
            }
        }

    }
}
