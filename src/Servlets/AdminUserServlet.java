package Servlets;

import Beans.Utilisateur;
import Beans.Vol;
import Utils.DBUtils;
import Utils.MyUtils;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

@WebServlet(urlPatterns = {"/admin_users"})
public class AdminUserServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //Forward à WEB-INF/views/homeView.jsp
        RequestDispatcher dispatcher;
        HttpSession session = request.getSession();
        Utilisateur utilisateur = MyUtils.getLoggedUser(session);
        Connection connection = MyUtils.getStoredConnection(request);
        try {
            List<Utilisateur> allUsers = DBUtils.allUsers(connection);
            request.setAttribute("allUsers", allUsers);
        } catch (SQLException e) {
            e.printStackTrace();
        }
        if (utilisateur != null) {
            if (utilisateur.getTypeAdmin()) {
                dispatcher = this.getServletContext().getRequestDispatcher("/admin_users.jsp");
            } else {
                dispatcher = this.getServletContext().getRequestDispatcher("/login");
            }
            dispatcher.forward(request, response);
        } else this.getServletContext().getRequestDispatcher("/login");

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String nom = request.getParameter("nom");
        String prenom = request.getParameter("prenom");
        String email = request.getParameter("email");
        Connection conn = MyUtils.getStoredConnection(request);



        Utilisateur user = null;
        if (request.getParameter("modify_user") != null) {

            int modify_user = Integer.parseInt(request.getParameter("modify_user").trim());
            try {
                user = DBUtils.findUser(conn, modify_user);
            } catch (SQLException e) {
                e.printStackTrace();
            }

            if (nom.isEmpty()) {
                nom = user.getNom();
            }
            if (prenom.isEmpty()) {
                prenom = user.getPrenom();
            }
            if (email.isEmpty()) {
                email = user.getEmail();
            }
            try {
                DBUtils.modifyUser(conn, nom, prenom, email, modify_user);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        if (request.getParameter("delete_user") != null) {
            int delete_user = Integer.parseInt(request.getParameter("delete_user").trim());
            try {
                DBUtils.deleteUser(conn, delete_user);
                response.setIntHeader("Refresh", (int) 0.5);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    }
}


