package Utils;

import Beans.Reservation;
import Beans.Utilisateur;
import Beans.Vol;

import javax.servlet.ServletRequest;
import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.sql.Connection;
import java.util.ArrayList;
import java.util.List;

public class MyUtils {
    public static final String ATT_NAME_CONNECTION = "ATTRIBUTE_FOR_CONNECTION";
    public static final String ATT_FLIGHT_RESERVATION = "ATTRIBUTE_FOR_RESERVATION_FLIGHT";
    private static final String ATT_NAME_USER_NAME = "ATTRIBUTE_FOR_STORE_USER_NAME_IN_COOKIE";

    public static void storeConnection(ServletRequest request, Connection connection) {
        request.setAttribute(ATT_NAME_CONNECTION, connection);
    }

    public static Connection getStoredConnection(ServletRequest request) {
        Connection connection = (Connection) request.getAttribute(ATT_NAME_CONNECTION);
        return connection;
    }

    public static void storedLoggedUser(HttpSession session, Utilisateur loggedUser) {
        session.setAttribute("loggedUser", loggedUser);
    }

    public static void storedSearchResults(HttpSession session, Vol vol){
        session.setAttribute("vol",vol);
    }

    public static Utilisateur getLoggedUser(HttpSession session) {
        Utilisateur loggedUser = (Utilisateur) session.getAttribute("loggedUser");
        return loggedUser;
    }

    public static void storeUserCookie(HttpServletResponse response, Utilisateur user) {

        Cookie cookieUserName = new Cookie(ATT_NAME_USER_NAME, user.getEmail());
        //the cookie will last at most 1 day (converted to seconds)
        cookieUserName.setMaxAge(24 * 60 * 60);
        response.addCookie(cookieUserName);
    }

    public static void storeFlights(HttpSession session, List<Vol> flightList) {
        session.setAttribute("flightList", flightList);
    }

    public static String getEmailInCookie(HttpServletRequest request) {
        Cookie[] cookies = request.getCookies();
        if (cookies != null) {
            for (Cookie cookie : cookies) {
                if (ATT_NAME_USER_NAME.equals(cookie.getName())) {
                    return cookie.getValue();
                }
            }
        }
        return null;
    }

    public static void deleteUserCookie(HttpServletResponse response) {
        Cookie cookieUserName = new Cookie(ATT_NAME_USER_NAME, null);
        cookieUserName.setMaxAge(0);
        response.addCookie(cookieUserName);
    }

    public static void storeReservation(HttpSession session, Reservation reservation){
        session.setAttribute("reservation",reservation);

    }



}
