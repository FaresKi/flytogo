package Utils;

import Beans.Reservation;
import Beans.Utilisateur;
import Beans.Vol;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;


public class DBUtils {


    public static void createUser(Connection connection, String nom, String prenom, String email, String password) throws Exception {
        String sql = "INSERT INTO Utilisateur (NOM, PRENOM, ADMIN, EMAIL, PASSWORD) VALUES (?,?,0,?,AES_ENCRYPT(?,'secret'))";
        PreparedStatement pstm = connection.prepareStatement(sql);

        pstm.setString(1, nom);
        pstm.setString(2, prenom);
        pstm.setString(3, email);
        pstm.setString(4, password);
        pstm.executeUpdate();
    }


    public static Utilisateur findUser(Connection conn, String email, String password) throws Exception {

        String sql = "Select NUMERO_UTIL, Email,Nom, Prenom, Admin, AES_DECRYPT(PASSWORD,'secret') AS PASSWORD from utilisateur  where Email =" + "'" + email + "'";
        if (conn == null) {
            System.out.println("merde");
        }
        PreparedStatement pstm = conn.prepareStatement(sql);
        ResultSet rs = pstm.executeQuery();

        if (rs.next()) {
            int numUtilisateur = rs.getInt("NUMERO_UTIL");
            String nom = rs.getString("Nom");
            String prenom = rs.getString("Prenom");
            Boolean typeAdmin = rs.getBoolean("Admin");
            Blob ablob = rs.getBlob("Password");
            String decryptedPassword = new String(ablob.getBytes(1l, (int) ablob.length()));
            if (password.equals(decryptedPassword)) {
                Utilisateur user = new Utilisateur(numUtilisateur, nom, prenom, email, password, typeAdmin);
                return user;
            }
        }
        return null;
    }

    public static Utilisateur findUser(Connection conn, String email) throws SQLException {
        String sql = "Select Email,Nom, Prenom, Admin from utilisateur where Email =" + email;
        PreparedStatement pstm = conn.prepareStatement(sql);
        ResultSet rs = pstm.executeQuery();
        if (rs.next()) {
            int numUtilisateur = rs.getInt("NUMERO_UTIL");
            String nom = rs.getString("Nom");
            String prenom = rs.getString("Prenom");
            Boolean typeAdmin = rs.getBoolean("Admin");
            String password = rs.getString("Password");
            Utilisateur user = new Utilisateur(numUtilisateur, nom, prenom, email, password, typeAdmin);
            return user;
        }
        return null;
    }

    public static List<Vol> allFlights(Connection conn) throws SQLException {
        String sql = "SELECT * FROM vols ";
        PreparedStatement pstm = conn.prepareStatement(sql);
        ResultSet rs = pstm.executeQuery();
        List<Vol> flightList = new ArrayList<>();
        while (rs.next()) {
            int num_vol = rs.getInt("NUMERO_VOL");
            String aeroport_dep = rs.getString("AEROPORT_DEP");
            String aeroport_arr = rs.getString("AEROPORT_ARR");
            Date date_dep = rs.getDate("DATE_DEP");
            Date date_arr = rs.getDate("DATE_DEP");
            int nombrePassagers = rs.getInt("NOMBRE_PASSAGERS");
            int prix = rs.getInt("PRIX");
            Vol vol = new Vol(num_vol, aeroport_dep, aeroport_arr, date_dep, date_arr, nombrePassagers, prix);
            flightList.add(vol);
        }
        return flightList;

    }

    public static List<Utilisateur> allUsers(Connection conn) throws SQLException {
        String sql = "SELECT * FROM utilisateur";
        PreparedStatement pstm = conn.prepareStatement(sql);
        ResultSet rs = pstm.executeQuery();
        List<Utilisateur> userList = new ArrayList<>();
        while (rs.next()) {
            int num_util = rs.getInt("NUMERO_UTIL");
            String nom = rs.getString("NOM");
            String prenom = rs.getString("PRENOM");
            boolean typeAdmin = rs.getBoolean("ADMIN");
            String email = rs.getString("EMAIL");
            String password = rs.getString("Password");

            Utilisateur user = new Utilisateur(num_util, nom, prenom, email, password, typeAdmin);
            userList.add(user);
        }
        return userList;

    }

    public static List<Reservation> allReservations(Connection conn) throws SQLException {
        String sql = "SELECT * FROM Reservations";
        PreparedStatement pstm = conn.prepareStatement(sql);
        ResultSet rs = pstm.executeQuery();
        List<Reservation> allReservations = new ArrayList<>();
        while (rs.next()) {
            int num_client = rs.getInt(1);
            int num_vol = rs.getInt(2);
            int prix = rs.getInt(3);
            Vol vol = findFlight(conn, num_vol);
            Reservation reservation = new Reservation(num_client, vol, prix);
            allReservations.add(reservation);
        }
        return allReservations;

    }


    public static List<Vol> findFlights(Utilisateur user,Connection conn, String aeroport_dep, String aeroport_arr, Date date_dep, Date date_arr, int nbr_adultes, int nbr_enfants) throws SQLException {
        aeroport_dep = "\"" + aeroport_dep + "\"";
        aeroport_arr = "\"" + aeroport_arr + "\"";
        String sql = "SELECT * FROM VOLS WHERE AEROPORT_DEP =" + aeroport_dep + " and AEROPORT_ARR=" + aeroport_arr + " and DATE_DEP=? and DATE_ARR=? " +
                "and NUMERO_VOL not in (SELECT NUMERO_VOL FROM reservations WHERE NUMERO_CLIENT =" + user.getNumUtilsateur() + " ) ";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setDate(1, date_dep);
        pstm.setDate(2, date_arr);
        System.out.println(pstm.toString());
        ResultSet rs = pstm.executeQuery();
        List<Vol> flightList = new ArrayList<>();
        while (rs.next()) {
            if (rs.getInt("NOMBRE_PASSAGERS") > (nbr_adultes + nbr_enfants)) {

                Vol vol = new Vol(rs.getInt("NUMERO_VOL"), aeroport_dep, aeroport_arr, date_dep, date_arr, nbr_adultes + nbr_enfants, rs.getInt("PRIX"));
                System.out.println(vol.getNumero_vol());
                flightList.add(vol);
            }

        }
        return flightList;
    }

    public static List<Reservation> findReservations(Connection conn, int numClient) throws SQLException {
        String sql = "SELECT * FROM reservations INNER JOIN vols v on reservations.NUMERO_VOL = v.NUMERO_VOL where reservations.NUMERO_CLIENT=" + numClient;
        PreparedStatement pstm = conn.prepareStatement(sql);
        ResultSet rs = pstm.executeQuery();
        List<Reservation> reservations = new ArrayList<>();
        while (rs.next()) {
            Vol vol = new Vol(rs.getInt("NUMERO_VOL"), rs.getString("AEROPORT_DEP"), rs.getString("AEROPORT_ARR"), rs.getDate("DATE_DEP"), rs.getDate("DATE_ARR"), rs.getInt("NOMBRE_PASSAGERS"), rs.getInt("PRIX"));
            Reservation reservation = new Reservation(numClient, vol);
            reservations.add(reservation);
        }

        return reservations;
    }

    public static Reservation createReservation(Connection conn, int num_client, Vol vol, int passagers) throws SQLException {
        String sql = "INSERT INTO reservations VALUES (?,?,?)";
        PreparedStatement pstm = conn.prepareStatement(sql);
        int prix_vol = vol.getPrix() * passagers;
        System.out.println("Prix du vol : " + prix_vol);
        pstm.setInt(1, num_client);
        pstm.setInt(2, vol.getNumero_vol());
        pstm.setInt(3, prix_vol);
        pstm.executeUpdate();

        String sql_modify = "UPDATE vols SET NOMBRE_PASSAGERS = NOMBRE_PASSAGERS - ? WHERE NUMERO_VOL=?";
        pstm = conn.prepareStatement(sql_modify);
        pstm.setInt(1, passagers);
        pstm.setInt(2, vol.getNumero_vol());
        pstm.executeUpdate();

        return new Reservation(num_client, vol, prix_vol);
    }

    public static Vol findFlight(Connection conn, int row_id) throws SQLException {
        String sql = "SELECT * FROM vols WHERE NUMERO_VOL=?";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, row_id);
        ResultSet rs = pstm.executeQuery();
        while (rs.next()) {
            return new Vol(row_id, rs.getString("AEROPORT_DEP"), rs.getString("AEROPORT_ARR"), rs.getDate("DATE_DEP"), rs.getDate("DATE_ARR"), rs.getInt("NOMBRE_PASSAGERS"), rs.getInt("PRIX"));
        }
        return null;
    }

    public static void modifyUser(Connection conn, String nom, String prenom, String email, int num_util) throws SQLException {
        String sql = "UPDATE Utilisateur SET Nom=?,Prenom=?,Email=? WHERE NUMERO_UTIL=? ";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, nom);
        pstm.setString(2, prenom);
        pstm.setString(3, email);
        pstm.setInt(4, num_util);
        pstm.executeUpdate();

    }

    public static void modifyFlight(Connection conn, String aeroport_dep, String aeroport_arr, Date date_dep, Date date_arr, int prix_passager, int num_vol) throws SQLException {
        String sql = "UPDATE vols SET AEROPORT_DEP = ?, AEROPORT_ARR = ?, DATE_DEP = ?, DATE_ARR = ?, PRIX = ? WHERE NUMERO_VOL=?";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setString(1, aeroport_dep);
        pstm.setString(2, aeroport_arr);
        pstm.setDate(3, date_dep);
        pstm.setDate(4, date_arr);
        pstm.setInt(5, prix_passager);
        pstm.setInt(6,num_vol);
        System.out.println(pstm.toString());
        pstm.executeUpdate();

    }

    public static void deleteUser(Connection conn, int num_util) throws SQLException {

        String sql_before="SET FOREIGN_KEY_CHECKS=0";
        String sql = "DELETE FROM Utilisateur WHERE NUMERO_UTIL = ? ";
        String sql_reservations ="DELETE FROM Reservations WHERE NUMERO_CLIENT=?";
        String sql_after="SET FOREIGN_KEY_CHECKS=1";
        PreparedStatement pstm = conn.prepareStatement(sql_before);
        pstm.executeUpdate();
        pstm=conn.prepareStatement(sql);
        pstm.setInt(1, num_util);
        pstm.executeUpdate();
        pstm=conn.prepareStatement(sql_reservations);
        pstm.setInt(1, num_util);
        pstm.executeUpdate();
        pstm=conn.prepareStatement(sql_after);
        pstm.executeUpdate();
    }

    public static void deleteFlight(Connection conn, int num_vol) throws SQLException {
        String sql = "DELETE FROM vols WHERE NUMERO_VOL = ?";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1, num_vol);
        pstm.executeUpdate();
    }

    public static Utilisateur findUser(Connection conn, int num_util) throws SQLException {
        String sql = "SELECT * FROM Utilisateur WHERE NUMERO_UTIL=?";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1,num_util);
        ResultSet rs = pstm.executeQuery();
        Utilisateur user = null;
        while(rs.next()){
            String nom = rs.getString("NOM");
            String prenom = rs.getString("PRENOM");
            boolean typeAdmin = rs.getBoolean("ADMIN");
            String email = rs.getString("EMAIL");
            String password = rs.getString("Password");
            user=new Utilisateur(num_util,nom,prenom,email,password,typeAdmin);
        }
        return user;
    }

    public static void deleteReservation(Connection conn, int num_util, int num_vol) throws SQLException {
        String sql = "DELETE FROM Reservations WHERE NUMERO_CLIENT=? AND NUMERO_VOL=?";
        PreparedStatement pstm = conn.prepareStatement(sql);
        pstm.setInt(1,num_util);
        pstm.setInt(2,num_vol);
        System.out.println("SQL : " + sql);
        pstm.executeUpdate();
    }

}
